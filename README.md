# Alat za analitiku

U pitanju je alat za izveštaje / analitike/ statistike. Učitavali bi 
podatke iz fajla / datoteke, konvertovali tipove po potrebi,gde bi posle 
imali mogućnost prikazivanja raznih grafika i slično (crtanje 
raspodele,barplot, zavisnosti podataka i slično), eventualno i 
štampanje/ čuvanje izveštaja. Kao neki mini PowerBI.
A ako ostane vremena i mogućnosti implementirali bismo neke metode 
regresije / klasifikacije.


# Članovi:
 - <a href="https://gitlab.com/zekimilovan">Жељко Миловановић 84/2020</a>
 - <a href="https://gitlab.com/pavle_12">Павле Поњавић 162/2020</a>
 - <a href="https://gitlab.com/anaknezeviic">Ана Кнежевић 356/2022</a>
 - <a href="https://gitlab.com/lukaarambasic01">Лука Арамбашић 169/2020</a>
 - <a href="https://gitlab.com/milicasopalovic">Милица Шопаловић 175/2020</a>

# Video:
 - <a href = "https://youtu.be/GFkoMvMH-g0">Link do videa</a>

# Potrebno za pokretanje:
<ul>
<li>Programski jezik : C++17</li>
<li>Qt: 6.6+</li>
<li>Biblioteke: QtCharts</li>
</ul>

# Preuzimanje i pokretanje:
1. Otvoriti terminal u zeljenom direktorijumu
2. Klonirati projekat sa : git clone git@gitlab.com:matf-bg-ac-rs/course-rs/projects-2023-2024/alat-za-analitiku.git
3. Otvoriti QtCreator > Open Project > Nadji zeljeni projekat > Otvoriti "CMakeLists"
4. Pritisnuti dugme Run u donjem levom uglu ekrana ili koristiti kombinaciju tastatura CTRL+R.

