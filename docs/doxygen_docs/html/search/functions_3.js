var searchData=
[
  ['dataframe_0',['DataFrame',['../class_data_frame.html#a69a9dc47b7506b8062fd34aedacbf579',1,'DataFrame::DataFrame()'],['../class_data_frame.html#a937d6ba98f8e36884617ae5a27a253b6',1,'DataFrame::DataFrame(const QString &amp;path)']]],
  ['deducttype_1',['deductType',['../src_2dataframe_8cpp.html#a60fabb1b215a922d6707af1c8c52dc96',1,'dataframe.cpp']]],
  ['df_2',['df',['../class_dynamic_u_i.html#a43e462afdf6ae64e6e2e1bbc438e82b6',1,'DynamicUI']]],
  ['dragenterevent_3',['dragEnterEvent',['../class_file_import_window.html#acc179bd19dd776cde7d7872db27c73fe',1,'FileImportWindow']]],
  ['drawdummie_4',['drawDummie',['../class_plot_window.html#a79c8dbe9d0f08243c762f598511de109',1,'PlotWindow']]],
  ['drawgraph_5',['drawGraph',['../class_plot_window.html#aa0f63f051eaf790530d5bd6e9b8288f1',1,'PlotWindow']]],
  ['dropevent_6',['dropEvent',['../class_file_import_window.html#a68338c76ca24d2deaab300b11248322b',1,'FileImportWindow']]],
  ['dynamicui_7',['DynamicUI',['../class_dynamic_u_i.html#a811040091eba94cfbd1e1af1d7b10728',1,'DynamicUI::DynamicUI(QWidget *parent=nullptr)'],['../class_dynamic_u_i.html#a429935115b2fbebd9e5402046760dbc7',1,'DynamicUI::DynamicUI(QWidget *parent=nullptr, const QString &amp;filePath=&quot;&quot;)']]]
];
