var searchData=
[
  ['calculatestat_0',['calculateStat',['../class_utils.html#a349c9e701fea69008acb917ed2a595e6',1,'Utils']]],
  ['catch_5fconfig_5fmain_1',['CATCH_CONFIG_MAIN',['../tests_2dataframe_8cpp.html#a656eb5868e824d59f489f910db438420',1,'dataFrame.cpp']]],
  ['childwidget_2',['childWidget',['../class_t_container.html#a47db35c038b54b6f1a0d20a66984acc4',1,'TContainer']]],
  ['clone_3',['clone',['../class_t_container.html#af3ae6c9b388309dad4ca0b327a339fa6',1,'TContainer']]],
  ['cmakecxxcompilerid_2ecpp_4',['CMakeCXXCompilerId.cpp',['../_c_make_c_x_x_compiler_id_8cpp.html',1,'']]],
  ['coltype_5',['colType',['../class_data_frame.html#a02b9467a5970122bfd6567d7dff75aed',1,'DataFrame']]],
  ['comparedatapoints_6',['compareDataPoints',['../graphs_8cpp.html#a2ea5d8bd18f0485abe3d59bdeecfedea',1,'graphs.cpp']]],
  ['compiler_5fid_7',['COMPILER_ID',['../_c_make_c_x_x_compiler_id_8cpp.html#a81dee0709ded976b2e0319239f72d174',1,'CMakeCXXCompilerId.cpp']]],
  ['contentwidget_8',['contentWidget',['../class_dynamic_u_i.html#a51b816df63026acc58edae63541ffbc9',1,'DynamicUI']]],
  ['count_9',['count',['../class_data_frame.html#abec731f12d8c98a5091cec6d5dedcf27',1,'DataFrame']]],
  ['createdonutchart_10',['createDonutChart',['../class_graphs.html#ac07ff30a0dd7b98f5f271a2276f7241c',1,'Graphs']]],
  ['createlinechart_11',['createLineChart',['../class_graphs.html#aa164267ba246cb8f66d2ae7fd1128a4c',1,'Graphs']]],
  ['createpen_12',['createPen',['../class_graphs.html#a4d4bd4376aee6fda61291a2ed11b0db6',1,'Graphs']]],
  ['createpiechart_13',['createPieChart',['../class_graphs.html#afb4414bb0ed5b28e2c8acbae238679c5',1,'Graphs']]],
  ['createpieordonutchart_14',['createPieOrDonutChart',['../class_graphs.html#aa5a7231d718606df6aefc8ab8cceb4e5',1,'Graphs']]],
  ['createscatterchart_15',['createScatterChart',['../class_graphs.html#a9c735836e91cd94bd3e361b11e6a8ffe',1,'Graphs']]],
  ['createseriesanimation_16',['createSeriesAnimation',['../class_graphs.html#a0ea8fef69cfa31444c753300df93f443',1,'Graphs']]],
  ['currentchartcolor_17',['currentChartColor',['../class_plot_window.html#a9be24d5f41e72571c54f21ade05dddb7',1,'PlotWindow']]],
  ['cxx_5fstd_18',['CXX_STD',['../_c_make_c_x_x_compiler_id_8cpp.html#a34cc889e576a1ae6c84ae9e0a851ba21',1,'CMakeCXXCompilerId.cpp']]]
];
