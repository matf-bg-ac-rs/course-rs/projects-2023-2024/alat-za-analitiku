var searchData=
[
  ['value_5fcounts_0',['value_counts',['../class_statistic.html#ab6dc9c84427d01ce71a4e2eeb6592ffd',1,'Statistic']]],
  ['valuecounts_1',['valueCounts',['../class_var_vector.html#a8e76a53fd81763e3430dfe6a2c65170f',1,'VarVector']]],
  ['variance_2',['variance',['../class_statistic.html#af13af1605c12176a3175027e9a6ffccd',1,'Statistic']]],
  ['vars_3',['vars',['../class_var_vector.html#afce59d710ba8ee803c81da4711da1534',1,'VarVector']]],
  ['varvector_4',['VarVector',['../class_var_vector.html#a66ed8ee1b438c90e45b6a0a044e2ce14',1,'VarVector::VarVector()'],['../class_var_vector.html#a7a877932256a2d245df9b25ad8ba5f3e',1,'VarVector::VarVector(TYPE t, QVector&lt; QString &gt; vars)']]]
];
