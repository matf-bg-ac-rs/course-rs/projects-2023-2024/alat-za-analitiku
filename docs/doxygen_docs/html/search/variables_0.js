var searchData=
[
  ['_5fcbsender_0',['_cbSender',['../class_plot_window.html#aa796b6bd9d264abfebc6e4dea5dab12a',1,'PlotWindow::_cbSender'],['../class_stat_window.html#a124dcdda04616b22c732dacea1565c4d',1,'StatWindow::_cbSender']]],
  ['_5fdf_1',['_df',['../class_data_frame.html#aa89012037296043886e111bf68cb40b1',1,'DataFrame::_df'],['../class_dynamic_u_i.html#a929e5a6b56ff6451c267166bc9c5f46d',1,'DynamicUI::_df'],['../class_main_window.html#acee71f1842d548203e9fcc53d44510d9',1,'MainWindow::_df']]],
  ['_5ffilename_2',['_fileName',['../class_file_import_window.html#aa3f3d7345d0f4a3f75c7abf6b69a82e5',1,'FileImportWindow']]],
  ['_5ffilepath_3',['_filePath',['../class_dynamic_u_i.html#adade47eeabb865819db19d0c84c2bc50',1,'DynamicUI::_filePath'],['../class_main_window.html#a0af58c09897cc5d5c07039efae313787',1,'MainWindow::_filePath']]],
  ['_5fgraph_4',['_graph',['../class_plot_window.html#a100e2f1fb6e68394e58c7a0c2e9d4939',1,'PlotWindow']]],
  ['_5fparrentlayout_5',['_parrentLayout',['../class_plot_window.html#a4243d7b5cfbba332cdcb4742d93f4786',1,'PlotWindow::_parrentLayout'],['../class_stat_window.html#a6d8ee76aa81cfbe7e666b06a35ebf82a',1,'StatWindow::_parrentLayout']]],
  ['_5ft_6',['_t',['../class_var_vector.html#a46984280bc242a5b6823c5e95a074afc',1,'VarVector']]],
  ['_5ftargetparent_7',['_targetParent',['../class_plot_window.html#a86482ce971444422973ee4ec6c5d4fc6',1,'PlotWindow::_targetParent'],['../class_stat_window.html#a7e5a9704b02c1e416b07435b174fb43a',1,'StatWindow::_targetParent']]],
  ['_5fvars_8',['_vars',['../class_var_vector.html#a9fe11f65274cbcb2ed0e5277b4aa8d31',1,'VarVector']]]
];
