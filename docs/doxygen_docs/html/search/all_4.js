var searchData=
[
  ['data_0',['data',['../class_plot_window.html#a85d426a0ea78a7720af5f3b72eabd6a2',1,'PlotWindow::data'],['../class_stat_window.html#a21e5c765180fb38c7ed177c8fe6b5bcd',1,'StatWindow::data']]],
  ['dataframe_1',['DataFrame',['../class_data_frame.html',1,'DataFrame'],['../class_data_frame.html#a69a9dc47b7506b8062fd34aedacbf579',1,'DataFrame::DataFrame()'],['../class_data_frame.html#a937d6ba98f8e36884617ae5a27a253b6',1,'DataFrame::DataFrame(const QString &amp;path)']]],
  ['dataframe_2ecpp_2',['dataframe.cpp',['../src_2dataframe_8cpp.html',1,'']]],
  ['dataframe_2ecpp_3',['dataFrame.cpp',['../tests_2dataframe_8cpp.html',1,'']]],
  ['dataframe_2eh_4',['dataframe.h',['../dataframe_8h.html',1,'']]],
  ['datapoint_5',['DataPoint',['../struct_data_point.html',1,'']]],
  ['dec_6',['DEC',['../_c_make_c_x_x_compiler_id_8cpp.html#ad1280362da42492bbc11aa78cbf776ad',1,'CMakeCXXCompilerId.cpp']]],
  ['deducttype_7',['deductType',['../src_2dataframe_8cpp.html#a60fabb1b215a922d6707af1c8c52dc96',1,'dataframe.cpp']]],
  ['df_8',['df',['../class_dynamic_u_i.html#a43e462afdf6ae64e6e2e1bbc438e82b6',1,'DynamicUI']]],
  ['dragenterevent_9',['dragEnterEvent',['../class_file_import_window.html#acc179bd19dd776cde7d7872db27c73fe',1,'FileImportWindow']]],
  ['drawdummie_10',['drawDummie',['../class_plot_window.html#a79c8dbe9d0f08243c762f598511de109',1,'PlotWindow']]],
  ['drawgraph_11',['drawGraph',['../class_plot_window.html#aa0f63f051eaf790530d5bd6e9b8288f1',1,'PlotWindow']]],
  ['dropevent_12',['dropEvent',['../class_file_import_window.html#a68338c76ca24d2deaab300b11248322b',1,'FileImportWindow']]],
  ['dynamicui_13',['DynamicUI',['../class_dynamic_u_i.html',1,'DynamicUI'],['../class_dynamic_u_i.html#a811040091eba94cfbd1e1af1d7b10728',1,'DynamicUI::DynamicUI(QWidget *parent=nullptr)'],['../class_dynamic_u_i.html#a429935115b2fbebd9e5402046760dbc7',1,'DynamicUI::DynamicUI(QWidget *parent=nullptr, const QString &amp;filePath=&quot;&quot;)']]],
  ['dynamicui_2ecpp_14',['dynamicui.cpp',['../dynamicui_8cpp.html',1,'']]],
  ['dynamicui_2eh_15',['dynamicui.h',['../dynamicui_8h.html',1,'']]]
];
