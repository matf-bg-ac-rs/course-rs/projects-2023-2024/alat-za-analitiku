var searchData=
[
  ['value_5fcounts_0',['value_counts',['../class_statistic.html#ab6dc9c84427d01ce71a4e2eeb6592ffd',1,'Statistic']]],
  ['value_5fcounts_2ecpp_1',['value_counts.cpp',['../value__counts_8cpp.html',1,'']]],
  ['valuecounts_2',['valueCounts',['../class_var_vector.html#a8e76a53fd81763e3430dfe6a2c65170f',1,'VarVector']]],
  ['variance_3',['variance',['../class_statistic.html#af13af1605c12176a3175027e9a6ffccd',1,'Statistic']]],
  ['variance_2ecpp_4',['variance.cpp',['../variance_8cpp.html',1,'']]],
  ['vars_5',['vars',['../class_var_vector.html#afce59d710ba8ee803c81da4711da1534',1,'VarVector']]],
  ['varvector_6',['VarVector',['../class_var_vector.html',1,'VarVector'],['../class_var_vector.html#a66ed8ee1b438c90e45b6a0a044e2ce14',1,'VarVector::VarVector()'],['../class_var_vector.html#a7a877932256a2d245df9b25ad8ba5f3e',1,'VarVector::VarVector(TYPE t, QVector&lt; QString &gt; vars)']]],
  ['varvector_2ecpp_7',['varVector.cpp',['../var_vector_8cpp.html',1,'']]]
];
