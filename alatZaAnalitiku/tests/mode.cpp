#include "catch.hpp"
#include "../src/statistic.h"

TEST_CASE("mode", "[function]") {

    SECTION("Calculating mode of vector with single mode") {
        QVector<QString> input = {"3", "1", "4", "1", "5", "9"};
        QString output = Statistic::mode(input);

        REQUIRE(output == "1");
    }

    SECTION("Calculating mode of vector with multiple modes") {
        QVector<QString> input = {"3", "1", "4", "1", "5", "9", "3", "4", "1"};
        QString output = Statistic::mode(input);

        REQUIRE(output == "1");
    }

    SECTION("Mode of empty vector should be empty string") {
        QVector<QString> input = {};
        QString output = Statistic::mode(input);

        REQUIRE(output.isEmpty());
    }
}
