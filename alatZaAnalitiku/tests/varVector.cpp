#include "catch.hpp"
#include "../src/dataframe.h"

TEST_CASE("VarVector tests") {
    SECTION("Creating an empty VarVector") {
        VarVector varVec;

        REQUIRE(varVec.vars().empty());
        REQUIRE(varVec.type() == TYPE::STRING);
    }

    SECTION("Pushing values and accessing VarVector") {
        QVector<QString> values = {"Value1", "Value2", "Value3"};
        VarVector varVec(TYPE::STRING, values);

        REQUIRE(varVec.vars().size() == 3);
        REQUIRE(varVec.vars()[0] == "Value1");
        REQUIRE(varVec.type() == TYPE::STRING);
    }

    SECTION("Getting value counts") {
        QVector<QString> values = {"A", "B", "A", "C", "A"};
        VarVector varVec(TYPE::STRING, values);

        std::unordered_map<QString, int> counts = varVec.valueCounts();

        REQUIRE(counts.size() == 3);
        REQUIRE(counts["A"] == 3);
        REQUIRE(counts["B"] == 1);
        REQUIRE(counts["C"] == 1);
    }
}
