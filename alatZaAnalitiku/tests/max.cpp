#include "catch.hpp"
#include "../src/statistic.h"

TEST_CASE("max", "[function]") {

    SECTION("Function returns the maximum from a vector of integers") {
        QVector<QString> input = {"3", "1", "4", "1", "5", "9"};
        double output = Statistic::max(input);

        REQUIRE(output == 9);
    }

    SECTION("Function returns the maximum from a vector of floats") {
        QVector<QString> input = {"3.5", "2.5", "1.5", "4.5"};
        double output = Statistic::max(input);

        REQUIRE(output == Approx(4.5));
    }

    SECTION("For empty vector function returns nan value") {
        QVector<QString> input = {};
        double output = Statistic::max(input);

        REQUIRE(std::isnan(output));
    }
}
