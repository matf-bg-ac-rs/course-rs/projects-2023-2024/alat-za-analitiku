#include "catch.hpp"
#include "../src/statistic.h"

TEST_CASE("Value counts calculation", "[function]") {

    SECTION("For empty data returns an empty map") {
        QVector<QString> emptyData;
        auto result = Statistic::value_counts(emptyData);
        REQUIRE(result.empty());
    }

    SECTION("For a vector of values returns a map that counts appearances of values in vector") {
        QVector<QString> testData = { "apple", "banana", "apple", "orange", "banana", "apple" };
        auto result = Statistic::value_counts(testData);

        REQUIRE(result["apple"] == 3);
        REQUIRE(result["banana"] == 2);
        REQUIRE(result["orange"] == 1);

        REQUIRE(result.find("grape") == result.end());
    }

}
