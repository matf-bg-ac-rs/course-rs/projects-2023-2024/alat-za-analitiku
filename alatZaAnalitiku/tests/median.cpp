#include "catch.hpp"
#include "../src/statistic.h"

TEST_CASE("median", "[function]") {

    SECTION("Calculating median of odd sized vector") {
        QVector<QString> input = {"3", "1", "4", "1", "5"};
        double output = Statistic::median(input);

        REQUIRE(output == Approx(4.0));
    }

    SECTION("Calculating median of even-sized vector") {
        QVector<QString> input = {"3.5", "2.5", "1.5", "4.5"};
        double output = Statistic::median(input);

        REQUIRE(output == Approx(2.0));
    }

    SECTION("Median of empty vector should be nan") {
        QVector<QString> input = {};
        double output = Statistic::median(input);

        REQUIRE(std::isnan(output));
    }
}
