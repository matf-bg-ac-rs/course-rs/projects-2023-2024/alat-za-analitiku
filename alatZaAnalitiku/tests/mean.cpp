#include "catch.hpp"
#include "../src/statistic.h"

TEST_CASE("mean", "[function]") {

    SECTION("Function returns mean for vector of integers") {
        QVector<QString> input = {"3", "1", "4", "1", "5", "9"};
        double output = Statistic::mean(input);

        REQUIRE(output == Approx(3.83333));
    }

    SECTION("Function returns mean for vector of floats") {
        QVector<QString> input = {"3.5", "2.5", "1.5", "4.5"};
        double output = Statistic::mean(input);

        REQUIRE(output == Approx(3.0));
    }

    SECTION("For empty vector function returns nan value") {
        QVector<QString> input = {};
        double output = Statistic::mean(input);

        REQUIRE(std::isnan(output));
    }
}
