#include "catch.hpp"
#include "../src/statistic.h"

TEST_CASE("standard deviation", "[function]") {

    SECTION("Calculating standard deviation of integers") {
        QVector<QString> input = {"3", "1", "4", "1", "5", "9"};
        double output = Statistic::sd(input);

        REQUIRE(output == Approx(2.7335365778).epsilon(0.001));
    }

    SECTION("Calculating standard deviation of floats") {
        QVector<QString> input = {"3.5", "2.5", "1.5", "4.5"};
        double output = Statistic::sd(input);

        REQUIRE(output == Approx(1.1180339887).epsilon(0.001));
    }

    SECTION("Standard deviation of empty vector should be nan") {
        QVector<QString> input = {};
        double output = Statistic::sd(input);

        REQUIRE(std::isnan(output));
    }
}
