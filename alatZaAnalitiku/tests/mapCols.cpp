#include "catch.hpp"
#include "../src/readcsv.h"

TEST_CASE("mapCols", "[function]") {

    SECTION("For empty headers: Expected empty mapping") {
        QVector<QString> headers = {};

        std::unordered_map<int, QString> mapped = ReadCSV::mapCols(headers);

        REQUIRE(mapped.empty());
    }

    SECTION("For non-empty headers: Mapping created successfully") {
        QVector<QString> headers = {"Header1", "Header2", "Header3"};

        std::unordered_map<int, QString> mapped = ReadCSV::mapCols(headers);

        REQUIRE(mapped.size() == headers.size());

        for (int i = 0; i < headers.size(); ++i) {
            REQUIRE(mapped[i] == headers[i]);
        }
    }
}
