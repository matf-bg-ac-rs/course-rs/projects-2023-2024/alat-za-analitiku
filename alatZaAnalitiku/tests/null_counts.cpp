#include "catch.hpp"
#include "../src/statistic.h"

TEST_CASE("Null counts calculation", "[null_count]") {
    Statistic statistic;

    SECTION("For empty data returns zero") {
        QVector<QString> emptyData;
        auto result = statistic.null_count(emptyData);
        REQUIRE(result == 0);
    }

    SECTION("In a vector of values counts null values") {
        QVector<QString> testData = { "apple", "", "banana", "", "orange", "" };
        auto result = statistic.null_count(testData);

        REQUIRE(result == 3);
    }

    SECTION("For a vector with no null values returns zero") {
        QVector<QString> testData = { "apple", "banana", "orange" };
        auto result = statistic.null_count(testData);

        REQUIRE(result == 0); 
    }

}
