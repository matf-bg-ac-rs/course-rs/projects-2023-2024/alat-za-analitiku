#include "catch.hpp"
#include "../src/statistic.h"

TEST_CASE("sum", "[function]") {

    SECTION("Function calculates sum of integers for vector of integers") {
        QVector<QString> input = {"3", "1", "4", "1", "5", "9"};
        double output = Statistic::sum(input);

        REQUIRE(output == Approx(23));
    }

    SECTION("Function calculates sum of floats for vector of floats") {
        QVector<QString> input = {"3.5", "2.5", "1.5", "4.5"};
        double output = Statistic::sum(input);

        REQUIRE(output == Approx(12.0));
    }

    SECTION("Sum of empty vector should be 0") {
        QVector<QString> input = {};
        double output = Statistic::sum(input);

        REQUIRE(output == Approx(0.0));
    }
}
