#include "catch.hpp"
#include "../src/statistic.h"

TEST_CASE("min", "[function]") {

    SECTION("Function returns the minimum from a vector of integers") {
        QVector<QString> input = {"3", "1", "4", "1", "5", "9"};
        double output = Statistic::min(input);

        REQUIRE(output == 1);
    }

    SECTION("Function returns the minimum from a vector of floats") {
        QVector<QString> input = {"3.5", "2.5", "1.5", "4.5"};
        double output = Statistic::min(input);

        REQUIRE(output == Approx(1.5));
    }

    SECTION("Function returns nan value for empty vector") {
        QVector<QString> input = {};
        double output = Statistic::min(input);

        REQUIRE(std::isnan(output));
    }
}
