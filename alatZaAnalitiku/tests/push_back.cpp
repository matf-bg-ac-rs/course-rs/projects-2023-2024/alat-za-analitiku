#include "catch.hpp"
#include "../src/dataframe.h"

TEST_CASE("DataFrame push_back method") {
    SECTION("Pushing a value to a new column") {
        DataFrame df;
        QString columnName = "Column1";
        QString value = "TestValue";

        df.push_back(columnName, value);

        REQUIRE(df.header().size() == 1);
        REQUIRE(df.header()[0] == columnName);

        VarVector col = df[columnName];
        REQUIRE(col.vars().size() == 1);
        REQUIRE(col.vars()[0] == value);
    }

    SECTION("Pushing a value to an existing column") {
        DataFrame df;
        QString columnName = "Column1";
        QString value1 = "Value1";
        QString value2 = "Value2";

        df.push_back(columnName, value1);
        df.push_back(columnName, value2);

        VarVector col = df[columnName];
        REQUIRE(col.vars().size() == 2);
        REQUIRE(col.vars()[0] == value1);
        REQUIRE(col.vars()[1] == value2);
    }
}
