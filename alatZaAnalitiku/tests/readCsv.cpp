#include "catch.hpp"
#include "../src/readcsv.h"
#include<QFile>
#include<QTextStream>

TEST_CASE("readCsv", "[function]") {

    SECTION("For non-existent file function throws nonExistentFilePath") {
        QString nonExistentFilePath = "non_existent_file.csv";

        REQUIRE_THROWS_WITH(
            ReadCSV::readCsv(nonExistentFilePath),
            "FILE DOES NOT EXIST"
            );
    }

    SECTION("For empty file function returns empty result") {

        QString emptyFilePath = "empty_file.csv";
        QFile emptyFile(emptyFilePath);
        if (!emptyFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
            FAIL("Failed to create an empty file for testing");
        }
        emptyFile.close();

        std::unordered_map<QString, QVector<QString>> result = ReadCSV::readCsv(emptyFilePath);

        REQUIRE(result.empty());
    }

    SECTION("Function returns valid data from the valid file") {
        QString validFilePath = "valid_file.csv";
        QFile validFile(validFilePath);
        if (!validFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
            FAIL("Failed to create a valid file for testing");
        }

        QTextStream out(&validFile);
        out << "Header1,Header2,Header3\n";
        out << "Value1,Value2,Value3\n";
        validFile.close();

        std::unordered_map<QString, QVector<QString>> result = ReadCSV::readCsv(validFilePath);

        REQUIRE(result.size() == 3);
        REQUIRE(result["Header1"].size() == 1);
        REQUIRE(result["Header1"][0] == "Value1");
        REQUIRE(result["Header2"][0] == "Value2");
        REQUIRE(result["Header3"][0] == "Value3");
    }
}

