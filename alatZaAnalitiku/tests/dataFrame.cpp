#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "../src/dataframe.h"

TEST_CASE("DataFrame tests") {
    SECTION("Creating an empty DataFrame") {
        DataFrame df;

        REQUIRE(df.header().empty());
    }

    SECTION("Pushing values and accessing DataFrame") {
        DataFrame df;
        QString columnName = "Column1";
        QString value = "TestValue";

        df.push_back(columnName, value);

        REQUIRE(df.header().size() == 1);
        REQUIRE(df.header()[0] == columnName);

        VarVector col = df[columnName];
        REQUIRE(col.vars().size() == 1);
        REQUIRE(col.vars()[0] == value);
    }

    SECTION("Getting column type") {
        DataFrame df;
        QString columnName = "NumericColumn";
        QString numericValue = "123";

        df.push_back(columnName, numericValue);
        df.setColType(columnName, TYPE::NUMERIC);

        REQUIRE(df.colType(columnName) == TYPE::NUMERIC);
    }

    SECTION("Filtering DataFrame by column value") {
        DataFrame df;
        QString columnName = "Column1";
        QString value1 = "Value1";
        QString value2 = "Value2";

        df.push_back(columnName, value1);
        df.push_back(columnName, value2);

        DataFrame filteredDf = df.filter(columnName, value1);

        REQUIRE(filteredDf.header().size() == 1);
        REQUIRE(filteredDf.header()[0] == columnName);

        VarVector col = filteredDf[columnName];
        REQUIRE(col.vars().size() == 1);
        REQUIRE(col.vars()[0] == value1);
    }
}
