#include "catch.hpp"
#include "../src/statistic.h"

TEST_CASE("Variance calculation", "[function]") {

    SECTION("For empty data retuns nan") {
        QList<QString> emptyData = {};
        double result = Statistic::variance(emptyData);
        REQUIRE(std::isnan(result));
    }

    SECTION("For single value returns zero") {
        QList<QString> singleData = {"5.0"};
        double result = Statistic::variance(singleData);
        REQUIRE(result == 0.0);
    }

    SECTION("For multiple values returns correct variance") {
        QList<QString> testData = {"3.0", "6.0", "9.0", "12.0"};
        double result = Statistic::variance(testData);
        REQUIRE(result == 11.25);
    }
}
